package ui;


import primary.pojo.Book;
import primary.pojo.User;
import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.service.MangerOperator;
import primary.utils.TipUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class MangerPanel {
    private User user;
    private MangerOperator mangerOperator;
    public MangerPanel(User user) {
        this.user = user;
        mangerOperator=new MangerOperator(user);
    }

    public void addBook(){
        JFrame f = new JFrame("添加书籍");
        JPanel jPanel = new JPanel();
        Dimension dimension = new Dimension(100, 30);
        Font font = new Font("宋体", Font.PLAIN, 14);
        JLabel l_name= new JLabel("图书名字");
        l_name.setFont(font);

        JTextField t_name = new JTextField();
        t_name.setPreferredSize(dimension);

        JLabel l_type= new JLabel("类型");
        l_type.setFont(new Font("宋体",Font.PLAIN,14));
        JComboBox<String> t_type= new JComboBox<>();
        t_type.addItem("教材");
        t_type.addItem("专业书籍");
        t_type.addItem("文艺作品");
        t_type.addItem("个人传记");
        t_type.addItem("其他");

        JLabel l_total= new JLabel("图书总量");
        l_total.setFont(font);

        JTextField t_total = new JTextField();
        t_total.setPreferredSize(dimension);
        JButton button1 = new JButton("添加");
        JButton button2 = new JButton("取消");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ReturnObject returnObject = null;

                try {
                    returnObject=  mangerOperator.addBook(t_name.getText(), t_type.getSelectedItem().toString(), Integer.parseInt(t_total.getText()));
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统出错",true);
                }
                if(returnObject.getCode()== Contants.USER_ERROR){
                    TipUtil.popWindowTip(returnObject.getMsg(),true);
                }
                if(returnObject.getCode()== Contants.USER_SUCCESS){
                    f.dispose();
                    TipUtil.popWindowTip(returnObject.getMsg(),false);
                }
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(l_type);
        jPanel.add(t_type);
        jPanel.add(l_total);
        jPanel.add(t_total);
        jPanel.add(button1);
        jPanel.add(button2);
        f.add(jPanel);
        f.setSize(700,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
    public void delBook(){
        JFrame f = new JFrame("删除图书");
        JPanel jPanel = new JPanel();
        JLabel l_name= new JLabel("书的名称");
        f.setLocationRelativeTo(null);

        l_name.setFont(new Font("宋体",Font.PLAIN,14));
        JTextField t_name = new JTextField();
        Dimension dimension = new Dimension(100, 30);
        t_name.setPreferredSize(dimension);
        JButton button1 = new JButton("确认删除");
        JButton button2 = new JButton();
        button2.setText("取消");


        button1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ReturnObject returnObject=null;
                try {
                    returnObject=  mangerOperator.delBook(t_name.getText());
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统错误",true);
                }

                if(returnObject.getCode()==Contants.USER_ERROR) TipUtil.popWindowTip(returnObject.getMsg(),true);
                else if(returnObject.getCode()==Contants.USER_SUCCESS){
                    f.dispose();
                    TipUtil.popWindowTip(returnObject.getMsg(),false);
                }
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(button1);
        jPanel.add(button2);

        f.add(jPanel);
        f.setSize(600,200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void selectBook(){
        JFrame f = new JFrame("查找图书");
        JPanel jPanel = new JPanel();
        JLabel l_name= new JLabel("先输入书的名称");
        f.setLocationRelativeTo(null);

        l_name.setFont(new Font("宋体",Font.PLAIN,14));
        JTextField t_name = new JTextField();
        Dimension dimension = new Dimension(100, 30);
        t_name.setPreferredSize(dimension);
        JButton button1 = new JButton("确认");
        JButton button2 = new JButton();
        button2.setText("取消");


        button1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ReturnObject returnObject=null;
                try {
                    returnObject=  mangerOperator.getBookByName(t_name.getText());
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统错误",true);
                }

                if(returnObject.getCode()==Contants.USER_ERROR) TipUtil.popWindowTip(returnObject.getMsg(),true);
                else if(returnObject.getCode()==Contants.USER_SUCCESS){
                    f.dispose();

                   updateBook((Book) returnObject.getData());
                }
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(button1);
        jPanel.add(button2);

        f.add(jPanel);
        f.setSize(600,200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
    public void updateBook(Book book){
        JFrame f = new JFrame("修改书籍");
        JPanel jPanel = new JPanel();
        Dimension dimension = new Dimension(100, 30);
        Font font = new Font("宋体", Font.PLAIN, 14);
        JLabel l_name= new JLabel("图书名字");
        l_name.setFont(font);

        JTextField t_name = new JTextField();
        t_name.setPreferredSize(dimension);
        t_name.setText(book.getBookName());
        JLabel l_type= new JLabel("类型");
        l_type.setFont(new Font("宋体",Font.PLAIN,14));
        JComboBox<String> t_type= new JComboBox<>();
        t_type.addItem("教材");
        t_type.addItem("专业书籍");
        t_type.addItem("文艺作品");
        t_type.addItem("个人传记");
        t_type.addItem("其他");
        t_type.setSelectedItem(book.getBookType());
        JLabel l_total= new JLabel("图书总量");
        l_total.setFont(font);

        JTextField t_total = new JTextField();
        t_total.setPreferredSize(dimension);
        t_total.setText(book.getTotalNum().toString());
        JButton button1 = new JButton("确认修改");
        JButton button2 = new JButton("取消");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ReturnObject returnObject = null;

                try {
                    returnObject= mangerOperator.updateBookById(t_name.getText(), t_type.getSelectedItem().toString(), Integer.parseInt(t_total.getText()),book.getId());
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统出错",true);
                }
                if(returnObject.getCode()== Contants.USER_ERROR){

                    TipUtil.popWindowTip(returnObject.getMsg(),true);
                }
                if(returnObject.getCode()== Contants.USER_SUCCESS){
                    f.dispose();
                    TipUtil.popWindowTip(returnObject.getMsg(),false);
                }
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(l_type);
        jPanel.add(t_type);
        jPanel.add(l_total);
        jPanel.add(t_total);
        jPanel.add(button1);
        jPanel.add(button2);
        f.add(jPanel);
        f.setSize(700,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void getBooksOrderBySome(Boolean isTotal,Boolean isEva,Boolean isTimes){
        JFrame frame = new JFrame();
        frame.setTitle("图书信息");
        frame.setSize(900, 600);
        frame.setLocationRelativeTo(null);

        ReturnObject returnObject=null;
        try {
            returnObject=mangerOperator.getAllBooksByCondition(isTotal,isEva,isTimes);
        } catch (Exception ee) {
            TipUtil.popWindowTip("系统出错",true);
        }
        HashMap<String, Object> map = (HashMap<String, Object>) returnObject.getData();
        String[] x =(String[])map.get("x");
        String[][] y =(String[][])map.get("y");
        JTable table = new JTable(y,x);
        JScrollPane jsp = new JScrollPane(table);

        frame.add(jsp);
        frame.setDefaultCloseOperation(3);
        frame.setVisible(true);
    }
}
