package ui;

import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.pojo.User;
import primary.service.UserVerify;
import primary.utils.TipUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginShow {
    public void loginUi(){
        JFrame frame = new JFrame();

        frame.setTitle("登录页面");
        frame.setSize(400, 250);
        frame.setDefaultCloseOperation(3);
        frame.setLocationRelativeTo(null);
        Font font = new Font("宋体", Font.PLAIN, 14);
        frame.setFont(font);

        FlowLayout fl = new FlowLayout(FlowLayout.CENTER,10,10);
        frame.setLayout(fl);


        JLabel l_name = new JLabel("用户名：");
        l_name.setFont(font);
        frame.add(l_name);

        JTextField text_name = new JTextField();
        Dimension dim1 = new Dimension(300,30);
        text_name.setPreferredSize(dim1);
        frame.add(text_name);

        JLabel labpass = new JLabel("密码：");
        labpass.setFont(font);
        frame.add(labpass);
        JPasswordField text_password = new JPasswordField();

        text_password.setPreferredSize(dim1);
        frame.add(text_password);


        JLabel tipMsg = TipUtil.errorMsg();
        frame.add(tipMsg);

        JLabel rightMsg = TipUtil.rightMsg();


        frame.add(rightMsg);

        JButton button1 = new JButton();
        JButton button2 = new JButton("注册");
        Dimension dim2 = new Dimension(100,30);
        button1.setText("登录");
        button1.setFont(font);
        button2.setFont(font);
        button1.setSize(dim2);
        button2.setSize(dim2);

        button1.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        UserVerify verifyUser = new UserVerify(text_name.getText(), text_password.getText());
                        ReturnObject returnObject=null;
                        try {
                             returnObject = verifyUser.toLogin();
                        } catch (Exception ee) {

//                            tipMsg.setText("系统出错! 请检查环境 重新运行");
                        }
                        if(returnObject.getCode()==1||returnObject.getCode()==2){

                            if(returnObject.getCode()== Contants.MANGER_SUCCESS){
                                new MangerMenu((User) returnObject.getData()).ui();

                            }
                            else if(returnObject.getCode()==Contants.USER_SUCCESS){

                                new UserMenu((User) returnObject.getData()).ui(returnObject.getMsg());
                            }

                            /**
                             * 如需要登录成功提示，可打开,  然后把 frame.dispose()注掉 否则 无意义
                             *
                              rightMsg.setText(returnObject.getMsg());

                             */

                          frame.dispose();
                        }
                        else{
                         tipMsg.setText(returnObject.getMsg());
                        }
                    }
                }
        );


        button2.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new RegisterShow().reg();
                    }
                }
        );

        frame.add(button1);
        frame.add(button2);
        frame.setVisible(true);
    }
}
