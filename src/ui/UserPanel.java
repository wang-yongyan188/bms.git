package ui;

import primary.pojo.MsgSquare;
import primary.pojo.User;
import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.service.UserOperator;
import primary.utils.TipUtil;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
public class UserPanel {

    private UserOperator userOperator ;
    public UserPanel(User user) {
        userOperator=new UserOperator(user);
    }

    public void lendBook(){
        JFrame f = new JFrame("借阅书籍");
        JPanel jPanel = new JPanel();
        Dimension dimension = new Dimension(100, 30);

        JLabel l_name= new JLabel("图书名字");
        l_name.setFont(new Font("宋体",Font.PLAIN,14));
        JTextField t_name = new JTextField();
        t_name.setPreferredSize(dimension);


        JLabel l_group= new JLabel("所借月数");
        l_group.setFont(new Font("宋体",Font.PLAIN,14));
        JComboBox<String> box1 = new JComboBox<>();
        box1.addItem("0个月");
        box1.addItem("1个月");
        box1.addItem("2个月");

        JLabel l_day= new JLabel("所借天数");
        JComboBox<String> box2 = new JComboBox<>();
        for (int i=0;i<30;i++){
            if(i<10){
                box2.addItem("0"+i+"天");
            }
            else{
                box2.addItem(i+"天");
            }
        }


        JButton button1 = new JButton("借阅");
        JButton button2 = new JButton("取消");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int months= Integer.parseInt(box1.getSelectedItem().toString().substring(0,1));
                int days= Integer.parseInt(box2.getSelectedItem().toString().substring(0,2));

                ReturnObject returnObject=null;
                try {
                    returnObject = userOperator.lendBook(t_name.getText(), months, days);
                } catch (Exception ee) {

                    TipUtil.popWindowTip("系统错误",true);

                }
                if(returnObject.getCode()== Contants.USER_SUCCESS){
                    userOperator.setUser((User) returnObject.getData());
                    TipUtil.popWindowTip(returnObject.getMsg(),false);
                    f.dispose();
                }
                else{
                    TipUtil.popWindowTip(returnObject.getMsg(),true);
                }

                t_name.setText("");
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(l_group);
        jPanel.add(box1);
        jPanel.add(l_day);
        jPanel.add(box2);
        jPanel.add(button1);
        jPanel.add(button2);
        f.add(jPanel);
        f.setSize(700,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void sendBook(){
        JFrame f = new JFrame("归还图书");
        JPanel jPanel = new JPanel();
        JLabel l_name= new JLabel("书的名称");
        f.setLocationRelativeTo(null);

        l_name.setFont(new Font("宋体",Font.PLAIN,14));
        JTextField t_name = new JTextField();
        Dimension dimension = new Dimension(100, 30);
        t_name.setPreferredSize(dimension);
        JButton button1 = new JButton("确认归还");
        JButton button2 = new JButton();
        button2.setText("取消");


        button1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ReturnObject returnObject=null;

                    try {
                        returnObject= userOperator.sentBook(t_name.getText());
                    }
                    catch (Exception ee) {
                        TipUtil.popWindowTip("系统错误",true);
                    }
                    if(returnObject.getCode()==Contants.USER_SUCCESS) {
                        userOperator.setUser((User)returnObject.getData());


                        // TODO: 2022/10/6/0006
//                        TipUtil.popWindowTip(returnObject.getMsg(),false);
                            popToEvaluate(t_name.getText());

                        f.dispose();
                    }

                    if(returnObject.getCode()==Contants.USER_ERROR) TipUtil.popWindowTip(returnObject.getMsg(),true);
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(button1);
        jPanel.add(button2);

        f.add(jPanel);
        f.setSize(600,200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public void sigInToGetIntegral(){
        JFrame frame = new JFrame();
        frame.setTitle("每日签到");
        frame.setSize(500, 150);
        frame.setDefaultCloseOperation(3);
        frame.setLocationRelativeTo(null);
        FlowLayout fl = new FlowLayout(FlowLayout.CENTER,10,10);

        JLabel l_tip= new JLabel("积分+1");
        l_tip.setFont(new Font("宋体",Font.PLAIN,20));

        JButton button1 = new JButton();
        button1.setText("签到");
        JButton button2 = new JButton();
        button2.setText("取消");

        button1.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        ReturnObject returnObject=null;
                        try {
                             returnObject = userOperator.sigInToGetIntegral();
                        } catch (Exception ee) {
                            TipUtil.popWindowTip("系统出错",true);
                        }
                        if(returnObject.getCode()==Contants.USER_SUCCESS){
                            userOperator.setUser((User) returnObject.getData());
                            TipUtil.popWindowTip(returnObject.getMsg(),false);
                        }
                        else{
                            TipUtil.popWindowTip(returnObject.getMsg(),true);
                        }
                    }
                });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        frame.setLayout(fl);
        frame.add(l_tip);
        frame.add(button1);
        frame.add(button2);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void myInfo(){
        JFrame frame = new JFrame();
        frame.setTitle("我的消息");
        frame.setSize(400, 600);
        frame.setDefaultCloseOperation(3);
        frame.setLocationRelativeTo(null);
        Font font = new Font("宋体", Font.PLAIN, 14);
        frame.setFont(font);

        FlowLayout fl = new FlowLayout(FlowLayout.CENTER,10,10);
        frame.setLayout(fl);

        JButton button1 = new JButton("关闭");
        JButton button2 = new JButton("清空消息");
        Dimension dim2 = new Dimension(100,30);
        button1.setFont(font);
        button2.setFont(font);
        button1.setSize(dim2);
        button2.setSize(dim2);
        JTextArea t = new JTextArea(150,30);

        ReturnObject res=null;
        try {
             res = userOperator.getMyInfo();
        } catch (Exception e) {
            TipUtil.popWindowTip("系统错误",true);
        }
        if(res.getCode()==Contants.USER_ERROR){
            TipUtil.popWindowTip(res.getMsg(),true);
            return;
        }
        String[] data = (String[]) res.getData();
        for (String str:data) {
            t.append(str+"\n");
        }

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    userOperator.clearInfo();
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统出错",true);
                }
                frame.dispose();
                TipUtil.popWindowTip("清除成功",false);
            }
        });

        frame.add(button1);
        frame.add(button2);
        frame.add(t);
        frame.setVisible(true);
    }
    public void orderRecord(Boolean isProceed){
        JFrame frame = new JFrame();
        frame.setTitle("租借记录");
        frame.setSize(900, 600);
        frame.setLocationRelativeTo(null);

        ReturnObject orders=null;
        try {
            orders = userOperator.getOrders(isProceed);
        } catch (Exception ee) {
            TipUtil.popWindowTip("系统出错",true);
        }
        HashMap<String, Object> map = (HashMap<String, Object>) orders.getData();
        String[] x =(String[])map.get("x");
        String[][] y =(String[][])map.get("y");
        JTable table = new JTable(y,x);
        JScrollPane jsp = new JScrollPane(table);

        frame.add(jsp);
        frame.setDefaultCloseOperation(3);
        frame.setVisible(true);
    }

    public void delayReturnBook(){
        JFrame f = new JFrame("延期返回");
        JPanel jPanel = new JPanel();
        Dimension dimension = new Dimension(100, 30);

        JLabel l_name= new JLabel("图书名字");
        l_name.setFont(new Font("宋体",Font.PLAIN,14));
        JTextField t_name = new JTextField();
        t_name.setPreferredSize(dimension);


        JLabel l_day= new JLabel("延期天数");
        JComboBox<String> select_days = new JComboBox<>();
        for (int i=1;i<31;i++){
            if(i<10){
                select_days.addItem("0"+i+"天");
            }
            else{
                select_days.addItem(i+"天");
            }
        }


        JButton button1 = new JButton("确定延期归还");
        JButton button2 = new JButton("取消");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int days= Integer.parseInt(select_days.getSelectedItem().toString().substring(0,2));

                ReturnObject returnObject=null;

                try {
                   returnObject= userOperator.delayReturnBook(t_name.getText(),days);
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统出错",true);
                }
                Boolean isFail;
                isFail=returnObject.getCode()==Contants.USER_ERROR?true:false;
                if(!isFail) f.dispose();

                TipUtil.popWindowTip(returnObject.getMsg(),isFail);
                t_name.setText("");
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(l_day);
        jPanel.add(select_days);
        jPanel.add(button1);
        jPanel.add(button2);
        f.add(jPanel);
        f.setSize(700,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    /**
     * 评价图书
     */
    public void evaluateBook(String bookName){
        JFrame f = new JFrame("评价一下");
        JPanel jPanel = new JPanel();

        Dimension dimension = new Dimension(100, 30);
        Font font = new Font("宋体", Font.PLAIN, 14);

        JLabel l_name= new JLabel("书的名称");
        l_name.setFont(font);
        JTextField t_name = new JTextField();
        t_name.setText(bookName);
        t_name.setPreferredSize(dimension);

        JLabel l_grade = new JLabel("选择分数");
        JComboBox<Integer> t_grade = new JComboBox<>();
        for (int i=5;i>0;i--){
            t_grade.addItem(i);
        }

        JLabel l_content = new JLabel("输入评价");

        l_content.setFont(font);
        JTextArea t_content = new JTextArea(3, 20);
        t_content.setFont(new Font("宋体",Font.PLAIN,14));


        JButton j1 = new JButton("发表评价");
        j1.setFont(font);


        j1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int grade = Integer.parseInt(t_grade.getSelectedItem().toString());
                ReturnObject returnObject=null;
                try {
                    returnObject= userOperator.publishEvaluate(t_name.getText(), grade, t_content.getText());
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统错误",true);
                }
                if(returnObject.getCode()==Contants.USER_SUCCESS){
                    f.dispose();
                    TipUtil.popWindowTip("评价成功",false);
                }
                if(returnObject.getCode()==Contants.USER_ERROR){
                    TipUtil.popWindowTip(returnObject.getMsg(),false);
                }
            }
        });


        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(l_grade);
        jPanel.add(t_grade);
        jPanel.add(l_content);
        jPanel.add(t_content);

        jPanel.add(j1);

        f.add(jPanel);
        f.setSize(600,200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    private void popToEvaluate(String bookName){
        JFrame frame = new JFrame();

        frame.setTitle("成功");

        frame.setSize(500, 150);
        frame.setDefaultCloseOperation(3);
        frame.setLocationRelativeTo(null);
        FlowLayout fl = new FlowLayout(FlowLayout.CENTER,10,10);

        JLabel l_tip= new JLabel("归还成功，评价一下吧！");
        l_tip.setFont(new Font("宋体",Font.PLAIN,20));

        JButton button1 = new JButton();
        button1.setText("立即评价");

        JButton button2 = new JButton();
        button2.setText("取消");
        button1.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        frame.dispose();
                        evaluateBook(bookName);
                    }
                });
        button2.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        frame.dispose();
                    }
                });
        frame.setLayout(fl);
        frame.add(l_tip);
        frame.add(button1);
        frame.add(button2);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void changePasswd(){
        JFrame f = new JFrame("修改密码");
        JPanel jPanel = new JPanel();
        Font font = new Font("宋体", Font.PLAIN, 14);
        JPasswordField t_old = new JPasswordField();
        JPasswordField t_new = new JPasswordField();
        Dimension dimension = new Dimension(100, 30);
        JLabel l_old= new JLabel("旧密码");
        JLabel l_new= new JLabel("新密码");
        l_old.setFont(font);
        l_new.setFont(font);
        t_old.setPreferredSize(dimension);
        t_new.setPreferredSize(dimension);
        JButton j1 = new JButton("修改");
        JButton j2 = new JButton("取消");

        j1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ReturnObject returnObject =null;
                try {
                     returnObject = userOperator.changeUserPasswd(t_new.getText(), t_old.getText());
                } catch (Exception ee) {
                    TipUtil.popWindowTip("系统错误",true);
                }
                if(returnObject.getCode()==Contants.USER_ERROR){
                    TipUtil.popWindowTip(returnObject.getMsg(),true);
                }
                if(returnObject.getCode()==Contants.USER_SUCCESS){
                    TipUtil.popWindowTip(returnObject.getMsg(),false);
                    f.dispose();
                }
            }
        });
        j2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f.dispose();
            }
        });

        jPanel.add(l_old);
        jPanel.add(t_old);
        jPanel.add(l_new);
        jPanel.add(t_new);
        jPanel.add(j1);
        jPanel.add(j2);


        f.add(jPanel);
        f.setSize(500,200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

    public  void getMsgSquare(){
        JFrame frame = new JFrame();
        frame.setTitle("消息广场");
        frame.setSize(400, 600);
        frame.setDefaultCloseOperation(3);
        frame.setLocationRelativeTo(null);
        Font font = new Font("宋体", Font.PLAIN, 14);
        frame.setFont(font);

        FlowLayout fl = new FlowLayout(FlowLayout.CENTER,10,10);
        frame.setLayout(fl);

        JButton button1 = new JButton("关闭");
        Dimension dim2 = new Dimension(50,30);
        button1.setFont(font);
        button1.setSize(dim2);
        JTextArea t = new JTextArea(150,25);

        ReturnObject res=null;
        try {

            res =userOperator.getMsgSquare();

        } catch (Exception e) {
            TipUtil.popWindowTip("系统错误",true);
        }

        ArrayList<MsgSquare> list = (ArrayList<MsgSquare>) res.getData();
        for (MsgSquare msg:list) {
            t.append(msg.getTime()+" :  "+msg.getMsg()+"\n");
        }

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });

        frame.add(button1);
        frame.add(t);
        frame.add(button1);
        frame.setVisible(true);
    }

}
