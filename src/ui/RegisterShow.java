package ui;

import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.service.UserRegister;
import primary.utils.TipUtil;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegisterShow {
    public void reg(){
        JFrame f = new JFrame("注册普通用户");
        JPanel jPanel = new JPanel();

        JTextField t_name = new JTextField();
        JTextField t_passwd = new JTextField();
        Dimension dimension = new Dimension(100, 30);
        JLabel l_name= new JLabel("用户名");
        JLabel l_passd= new JLabel("密码");
        l_name.setFont(new Font("宋体",Font.PLAIN,14));
        l_passd.setFont(new Font("宋体",Font.PLAIN,14));
        t_name.setPreferredSize(dimension);
        t_passwd.setPreferredSize(dimension);
        JButton j1 = new JButton("注册");
        JLabel rightMsg = TipUtil.rightMsg();
        JLabel errorTip = TipUtil.errorMsg();

        j1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rightMsg.setText("");
                errorTip.setText("");
                ReturnObject returnObject=null;
                try {
                    returnObject = new UserRegister(t_name.getText(), t_passwd.getText()).toReg();
                    if(returnObject.getCode()== Contants.USER_ERROR){
                        errorTip.setText(returnObject.getMsg());
                    }
                    else if(returnObject.getCode()==Contants.USER_SUCCESS){
                        TipUtil.popWindowTip(returnObject.getMsg(),false);
                        f.dispose();
                    }
                    t_name.setText("");
                    t_passwd.setText("");


                } catch (Exception ee) {
                    errorTip.setText("系统出错,请检查运行环境");
                }
            }
        });
        jPanel.add(l_name);
        jPanel.add(t_name);
        jPanel.add(l_passd);
        jPanel.add(t_passwd);
        jPanel.add(j1);
        jPanel.add(rightMsg);
        jPanel.add(errorTip);

        f.add(jPanel);
        f.setSize(700,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }

}
