package ui;

import primary.pojo.User;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserMenu {
    public void setUser(User user) {
        this.user = user;
    }

    private User user;

    public UserMenu(User user) {
        this.user = user;
    }

    public void ui(String msg) {
        JFrame jframe = new JFrame(user.getName()+" 的主页  "+" 当前积分:"+user.getIntegral()+" 系统通知："+msg);
        jframe.setBounds(300, 180, 650, 500);
        JPanel jPanel = new JPanel();
        JMenuBar jmenuBar = new JMenuBar();
        JMenu sf = new JMenu("借阅图书");
        JMenu af = new JMenu("归还图书");
        JMenu df = new JMenu("个人中心");
        JMenu uf = new JMenu("续借图书");
        JMenu cf = new JMenu("评价图书");
        JMenu mf = new JMenu("消息广场");
        JMenuItem sf_1 = new JMenuItem("借阅");
        JMenuItem af_1 = new JMenuItem("归还");
        JMenuItem df_1 = new JMenuItem("查看所有借书记录");
        JMenuItem df_2 = new JMenuItem("查看借阅中图书");
        JMenuItem df_3 = new JMenuItem("签到领积分");
        JMenuItem df_4 = new JMenuItem("我的消息");
        JMenuItem df_6 = new JMenuItem("修改密码");
        JMenuItem df_5 = new JMenuItem("退出登录");
        JMenuItem uf_1 = new JMenuItem("续借");
        JMenuItem cf_1 = new JMenuItem("评价");
        JMenuItem mf_1 = new JMenuItem("进入广场");
        UserPanel userPanel = new UserPanel(this.user);

        sf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.lendBook();
            }
        });
        af_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.sendBook();
            }
        });
        df_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.orderRecord(false);
            }
        });
        df_2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.orderRecord(true);
            }
        });

        uf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.delayReturnBook();
            }
        });
        cf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.evaluateBook("");
            }
        });
        df_3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.sigInToGetIntegral();

            }
        });

        df_4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.myInfo();
            }
        });
        df_5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jframe.dispose();
                setUser(null);
                new LoginShow().loginUi();
            }
        });

        df_6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.changePasswd();

            }
        });
        mf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userPanel.getMsgSquare();
            }
        });

        af.add(af_1);
        sf.add(sf_1);
        df.add(df_1);
        df.add(df_2);
        df.add(df_3);
        df.add(df_4);
        df.add(df_6);
        df.add(df_5);
        uf.add(uf_1);
        cf.add(cf_1);
        mf.add(mf_1);
        jmenuBar.add(sf);
        jmenuBar.add(af);
        jmenuBar.add(df);
        jmenuBar.add(uf);
        jmenuBar.add(cf);
        jmenuBar.add(mf);
        jPanel.add(jmenuBar);
        jframe.add(jPanel);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
