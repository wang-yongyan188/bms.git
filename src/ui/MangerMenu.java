package ui;

import primary.pojo.User;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MangerMenu {
    private MangerPanel mangerPanel;
    private User user;
    public MangerMenu(User user) {
        this.mangerPanel=new MangerPanel(user);
        this.user=user;
    }

    public void ui() {

        JFrame jframe = new JFrame("管理员主页");
        jframe.setBounds(300, 180, 650, 500);
        JPanel jPanel = new JPanel();
        JMenuBar jmenuBar=new JMenuBar();
        JMenu sf = new JMenu("管理图书");
        JMenu mf = new JMenu("消息广场");
        JMenu lf = new JMenu("查看图书数据");
        JMenu qf = new JMenu("安全退出");

        JMenuItem sf_1 = new JMenuItem("添加书籍");
        JMenuItem sf_2 = new JMenuItem("修改书籍");
        JMenuItem sf_3 = new JMenuItem("下架书籍");
        JMenuItem mf_1 = new JMenuItem("进入广场");
        JMenuItem lf_1 = new JMenuItem("按照库存从少到多查询");
        JMenuItem lf_2 = new JMenuItem("按照评分从高到低查询");
        JMenuItem lf_3= new JMenuItem("按照借阅频次高到低查询");
        JMenuItem qf_1= new JMenuItem("安全退出");
        sf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mangerPanel.addBook();
            }
        });
        sf_2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mangerPanel.selectBook();
            }
        });
        sf_3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mangerPanel.delBook();
            }
        });
        mf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UserPanel(user).getMsgSquare();
            }
        });


        lf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mangerPanel.getBooksOrderBySome(true,false,false);
            }
        });
        lf_2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mangerPanel.getBooksOrderBySome(false,true,false);
            }
        });
        lf_3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mangerPanel.getBooksOrderBySome(false,false,true);
            }
        });
        qf_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               jframe.dispose();
            }
        });



        sf.add(sf_1);
        sf.add(sf_2);
        sf.add(sf_3);
        mf.add(mf_1);
        lf.add(lf_1);
        lf.add(lf_2);
        lf.add(lf_3);
        qf.add(qf_1);
        jmenuBar.add(sf);
        jmenuBar.add(mf);
        jmenuBar.add(lf);
        jmenuBar.add(qf);
        jPanel.add(jmenuBar);
        jframe.add(jPanel);
        jframe.setVisible(true);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
