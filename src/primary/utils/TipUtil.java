package primary.utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TipUtil {
    public static JLabel errorMsg(){
        JLabel tipMsg = new JLabel("");
        Font f = new Font("宋体",Font.BOLD,16);

        tipMsg.setFont(f);
        tipMsg.setForeground(Color.red);
        return tipMsg;
    }

    public static JLabel rightMsg(){
        JLabel tipMsg = new JLabel("");
        Font f = new Font("宋体",Font.BOLD,16);

        tipMsg.setFont(f);
        tipMsg.setForeground(Color.GREEN);
        return tipMsg;
    }
    public static void popWindowTip(String msg,Boolean isErr){
        JFrame frame = new JFrame();

        frame.setTitle(isErr?"失败":"成功");

        frame.setSize(500, 150);
        frame.setDefaultCloseOperation(3);
        frame.setLocationRelativeTo(null);
        FlowLayout fl = new FlowLayout(FlowLayout.CENTER,10,10);

        JLabel l_tip= new JLabel(msg);
        l_tip.setFont(new Font("宋体",Font.PLAIN,20));

        JButton button1 = new JButton();
        button1.setText("确定");


        button1.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        frame.dispose();
                    }
                });

        frame.setLayout(fl);
        frame.add(l_tip);
        frame.add(button1);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}