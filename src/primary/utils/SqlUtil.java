package primary.utils;
import primary.pojo.Book;
import primary.pojo.MsgSquare;
import primary.pojo.Order;
import primary.pojo.User;
import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SqlUtil {
    public static Connection getCon() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/libmanger", "root", "1234");
        return con;
    }

    public User getUserByName(String name) throws SQLException, ClassNotFoundException {
        Connection con = getCon();

        String sql="SELECT * from tbl_user WHERE NAME= ?";
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1,name);
        ResultSet resultSet = statement.executeQuery();

        if(!resultSet.next()) return null;

        User user = new User();

        user.setId(resultSet.getInt(1));
        user.setName(resultSet.getString(2));
        user.setPassword(resultSet.getString(3));
        user.setIsManger(resultSet.getInt(4));
        user.setRemark(resultSet.getString(5));
        user.setIntegral(resultSet.getInt(6));
        user.setLastSignTime(resultSet.getString(7));

        statement.close();
        con.close();

        return user;
    }

    public int addOrUpdateUser(User user,Boolean isAdd) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        User userByName = getUserByName(user.getName());
        String sql="";
        PreparedStatement statement=null;

        if(isAdd){
            if(userByName!=null){
                return 0;
            }
            sql="INSERT INTO tbl_user VALUES(NULL,?,?,0,'',0,'')";

            statement = con.prepareStatement(sql);
            statement.setString(1,user.getName());
            statement.setString(2,user.getPassword());
        }
        else{
           if(user.getPassword()==null){
               user.setPassword(userByName.getPassword());
           }
           if(user.getIsManger()==null){
               user.setIsManger(userByName.getIsManger());
           }
           if(user.getRemark()==null){
               user.setRemark(userByName.getRemark());
           }
           if(user.getIntegral()==null){
               user.setIntegral(userByName.getIntegral());
           }
           if(user.getLastSignTime()==null){
               user.setLastSignTime(userByName.getLastSignTime());
           }

            sql="UPDATE tbl_user SET password=?,is_manger=?,remark=? ,integral=?,last_sign_time= ?  where name= ? ";
            statement = con.prepareStatement(sql);

            statement.setString(1,user.getPassword());
            statement.setInt(2,user.getIsManger());
            statement.setString(3,user.getRemark());
            statement.setInt(4,user.getIntegral());

            statement.setString(5,user.getLastSignTime());
            statement.setString(6,user.getName());

        }

        int i = statement.executeUpdate();
        statement.close();

        con.close();
        return i;
    }

    public int createOneOrder(Order order,Book book) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        con.setAutoCommit(false);

        String sql="INSERT INTO tbl_order VALUES(?,?,?,?,?,'' ,?,-1,'',0,'',0,null)";

        PreparedStatement statement = con.prepareStatement(sql);

        book.setLendTimes(book.getLendTimes()+1);
        book.setLendNum(book.getLendNum()+1);

        addOrUpdateBook(book,false);
        statement.setString(1,order.getId());
        statement.setInt(2,order.getUserId());
        statement.setInt(3,order.getBookId());
        statement.setString(4,order.getBookName());
        statement.setString(5,order.getBorrowTime());

        statement.setString(6,order.getEndTime());
        int i = statement.executeUpdate();

        con.commit();

        statement.close();
        con.close();
        return i;
    }
    public int finishOneOrder(Order order,User user) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        if(addOrUpdateUser(user,false)!=1){
            throw new RuntimeException("finishOneOrder 修改用户失败");
        }
        Book book = getBookByName(order.getBookName());
        book.setLendNum(book.getLendNum()-1);
        if(addOrUpdateBook(book, false)!=1){
            throw new RuntimeException("finishOneOrder 更新书籍失败");
        }
        String sql="UPDATE tbl_order SET return_time=?,is_finish=1,remark=? WHERE id=?";
        PreparedStatement statement = con.prepareStatement(sql);

        statement.setString(1,order.getReturnTime());
        statement.setString(2,order.getRemark());
        statement.setString(3,order.getId());

        int i = statement.executeUpdate();

        statement.close();
        con.close();
        return i;
    }

    public Book getBookByName(String name) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        String sql="SELECT * FROM `tbl_book` where book_name=?";
        Book book = new Book();
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1,name);
        ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()){
            return null;
        }

        book.setId(resultSet.getInt(1));
        book.setBookName(resultSet.getString(2));
        book.setBookType(resultSet.getString(3));
        book.setTotalNum(resultSet.getInt(4));
        book.setLendNum(resultSet.getInt(5));
        book.setEvaluateTimes(resultSet.getInt(6));
        book.setEvaluateGrade(resultSet.getFloat(7));
        book.setLendTimes(resultSet.getInt(8));
        statement.close();
        con.close();
        return book;
    }
    public int addOrUpdateBook(Book book,Boolean isAdd) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        Book bookByName = getBookByName(book.getBookName());

        String sql="";
        PreparedStatement statement=null;

        if(isAdd){
            if(bookByName!=null){
                return 0;
            }
            sql="INSERT INTO tbl_book VALUES(NULL,?,?,?,0,0,NULL,0)";

            statement = con.prepareStatement(sql);

            statement.setString(1,book.getBookName());
            statement.setString(2,book.getBookType());
            statement.setInt(3,book.getTotalNum());
        }
        else{
            sql="UPDATE tbl_book " +
                    "SET book_type=?,total_num=?,lend_num=?,evaluate_grade=?, lend_times=? ,evaluate_times=? " +
                    "WHERE book_name=? ";


            statement = con.prepareStatement(sql);
            if(book.getBookType()==null){
                book.setBookType(bookByName.getBookType());
            }
            if(book.getTotalNum()==null){
                book.setTotalNum(bookByName.getTotalNum());
            }
            if(book.getLendNum()==null){
                book.setLendNum(bookByName.getLendNum());
            }
            if(book.getEvaluateGrade()==null){
                book.setEvaluateGrade(bookByName.getEvaluateGrade());
            }
            if(book.getLendTimes()==null){
                book.setLendTimes(bookByName.getLendTimes());
            }
            if(book.getEvaluateTimes()==null){
                book.setEvaluateTimes(bookByName.getEvaluateTimes());
            }


            statement.setString(1,book.getBookType());
            statement.setInt(2,book.getTotalNum());
            statement.setInt(3,book.getLendNum());
            statement.setFloat(4,book.getEvaluateGrade());
            statement.setInt(5,book.getLendTimes());
            statement.setInt(6,book.getEvaluateTimes());
            statement.setString(7,book.getBookName());
        }
        int i = statement.executeUpdate();
        statement.close();
        con.close();
        return i;
    }

    public List<Order> selectOrdersByUId(Integer id,Boolean isProceed) throws SQLException, ClassNotFoundException {
        Connection con = getCon();

        ArrayList<Order> orders = new ArrayList<>();
        String sql="SELECT * FROM `tbl_order`WHERE  user_id=? ";
        if(isProceed){
             sql="SELECT * FROM `tbl_order`WHERE  user_id=? and is_finish="+0;
        }
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()){
            Order order = new Order();
            order.setId(resultSet.getString(1));
            order.setUserId(resultSet.getInt(2));
            order.setBookId(resultSet.getInt(3));
            order.setBookName(resultSet.getString(4));
            order.setBorrowTime(resultSet.getString(5));
            order.setReturnTime(resultSet.getString(6));
            order.setEndTime(resultSet.getString(7));
            order.setEstimate(resultSet.getInt(8));
            order.setComment(resultSet.getString(9));
            order.setIsDelay(resultSet.getInt(10));
            order.setOriginTime(resultSet.getString(11));

            order.setIsFinish(resultSet.getInt(12));
            order.setRemark(resultSet.getString(13));
            orders.add(order);
        }
        return orders;
    }


    public int updateOrder(Order order) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        String sql="UPDATE tbl_order SET end_time=?,is_delay=?,estimate=?,`comment`=?,origin_time=?,remark=? WHERE id= ? ";
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1,order.getEndTime());
        statement.setInt(2,order.getIsDelay());
        statement.setInt(3,order.getEstimate());
        statement.setString(4,order.getComment());
        statement.setString(5,order.getOriginTime());

        statement.setString(6,order.getRemark());
        statement.setString(7,order.getId());

        int i = statement.executeUpdate();
        statement.close();
        con.close();
        return i;
    }
    public List<Order> selectOrdersByBooKName(String bookName,Boolean isProceed) throws SQLException, ClassNotFoundException {
        Connection con = getCon();

        ArrayList<Order> orders = new ArrayList<>();
        String sql="SELECT * FROM `tbl_order`WHERE  book_name=? ";
        if(isProceed){
            sql="SELECT * FROM `tbl_order`WHERE   book_name=? and is_finish="+0;
        }
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1,bookName);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){
            Order order = new Order();
            order.setId(resultSet.getString(1));
            order.setUserId(resultSet.getInt(2));
            order.setBookId(resultSet.getInt(3));
            order.setBookName(resultSet.getString(4));
            order.setBorrowTime(resultSet.getString(5));
            order.setReturnTime(resultSet.getString(6));
            order.setEndTime(resultSet.getString(7));
            order.setEstimate(resultSet.getInt(8));
            order.setComment(resultSet.getString(9));
            order.setIsDelay(resultSet.getInt(10));
            order.setOriginTime(resultSet.getString(11));

            order.setIsFinish(resultSet.getInt(12));
            order.setRemark(resultSet.getString(13));
            orders.add(order);
        }
        return orders;
    }

    public int delBook(String name) throws SQLException, ClassNotFoundException {
        Connection con = getCon();

        String sql="DELETE FROM tbl_book WHERE book_name=? ";
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1,name);

        int i = statement.executeUpdate();
        statement.close();
        con.close();
        return i;
    }
    public Book getBookById(Integer id) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        String sql="SELECT * FROM `tbl_book` where id=?";
        Book book = new Book();
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setInt(1,id);
        ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()){
            return null;
        }

        book.setId(resultSet.getInt(1));
        book.setBookName(resultSet.getString(2));
        book.setBookType(resultSet.getString(3));
        book.setTotalNum(resultSet.getInt(4));
        book.setLendNum(resultSet.getInt(5));
        book.setEvaluateTimes(resultSet.getInt(6));
        book.setEvaluateGrade(resultSet.getFloat(7));
        book.setLendTimes(resultSet.getInt(8));
        statement.close();
        con.close();
        return book;
    }
    public int addMsg(String msg) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        String sql="INSERT INTO tbl_msg_square VALUES(NULL,?,?)";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = sdf.format(new Date());
        PreparedStatement statement = con.prepareStatement(sql);
        statement.setString(1,nowTime);
        statement.setString(2,msg+";");
        int i = statement.executeUpdate();
        statement.close();
        con.close();
        return i;
    }

    public int updateBookById(Book book) throws SQLException, ClassNotFoundException {
        Connection con = getCon();

        String sql= "UPDATE tbl_book " +
                "SET book_name=?,book_type=?,total_num=? " +
                "WHERE id=? ";
        PreparedStatement statement= con.prepareStatement(sql);
        statement.setString(1,book.getBookName());
        statement.setString(2, book.getBookType());
        statement.setInt(3,book.getTotalNum());
        statement.setInt(4, book.getId());
        int i = statement.executeUpdate();
        statement.close();
        con.close();
        return i;
    }
    public List<MsgSquare>getMsg() throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        String sql="select * from tbl_msg_square";
        PreparedStatement statement = con.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        ArrayList<MsgSquare> msgLists = new ArrayList<>();

        while (resultSet.next()){
            MsgSquare msgSquare=new MsgSquare();
            msgSquare.setId(resultSet.getInt(1));
            msgSquare.setTime(resultSet.getString(2));
            msgSquare.setMsg(resultSet.getString(3));

            msgLists.add(msgSquare);
        }

        return msgLists;
    }
    public List<Book>getAllBooksByCondition(Boolean isTotal,Boolean isEva,Boolean isTimes) throws SQLException, ClassNotFoundException {
        Connection con = getCon();
        String sql="";
        if(isEva){
            sql= "SELECT * FROM tbl_book ORDER BY evaluate_grade desc";
        }
        if(isTimes){
           sql="SELECT * FROM tbl_book ORDER BY lend_times desc";
        }
        if(isTotal){
            sql="SELECT * FROM tbl_book ORDER BY total_num-lend_num asc";
        }

        PreparedStatement statement = con.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        ArrayList<Book> books = new ArrayList<>();

        while (resultSet.next()){
            Book book = new Book();
            book.setBookName(resultSet.getString(2));
            book.setBookType(resultSet.getString(3));

            book.setTotalNum(resultSet.getInt(4));
            book.setLendNum(resultSet.getInt(5));
            book.setEvaluateTimes(resultSet.getInt(6));
            book.setEvaluateGrade(resultSet.getFloat(7));
            book.setLendTimes(resultSet.getInt(8));
            books.add(book);
        }

        return books;
    }

}