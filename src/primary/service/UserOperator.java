package primary.service;

import primary.pojo.Book;
import primary.pojo.MsgSquare;
import primary.pojo.Order;
import primary.pojo.User;
import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.utils.SqlUtil;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class UserOperator {
    private User user;
    private SqlUtil sqlUtil;


    public void setUser(User user) {
        this.user = user;
    }

    public UserOperator(User user) {
        this.user = user;
        sqlUtil=new SqlUtil();
    }

    /**
     * 借书功能
     */
    public ReturnObject lendBook(String bookName,Integer months,Integer days) throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if(" ".equals(bookName)){
            returnObject.setMsg("请输入书名");
            return returnObject;
        }
        if(months==0 && days==0){
            returnObject.setMsg("至少选择一天");
            return returnObject;
        }

// TODO: 从数据库中重新查询，避免用户长期登录，数据不及时
        // TODO:  后来通过每次执行有关积分的操作 ，都会重新set 一下 user 即是更新 也可不用再次查询

        if(sqlUtil.getUserByName(user.getName()).getIntegral()<0){
            returnObject.setMsg("积分过低，暂时无法借阅");
            return returnObject;
        }


        List<Order> orders = sqlUtil.selectOrdersByUId(this.user.getId(), true);
        for (Order o:orders) {
            if(bookName.equals(o.getBookName())){
                returnObject.setMsg("借阅失败，你已借过此书");
                return returnObject;
            }
        }

        Book bookByName = sqlUtil.getBookByName(bookName);
        if(bookByName==null){
            returnObject.setMsg("图书馆中暂时没有此书");
            return returnObject;
        }

        if(bookByName.getLendNum()>=bookByName.getTotalNum()){
            returnObject.setMsg("图书已借光，请等待");
            return returnObject;
        }


        Order order = new Order();
        order.setId(UUID.randomUUID().toString());
        order.setUserId(user.getId());
        order.setBookId(bookByName.getId());
        order.setBookName(bookByName.getBookName());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String borrowTime = sdf.format(new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH,months);
        calendar.add(Calendar.DAY_OF_MONTH,days);
        String endTime = sdf.format(calendar.getTime());
        order.setBorrowTime(borrowTime);
        order.setEndTime(endTime);
        order.setIsFinish(0);

        int i = sqlUtil.createOneOrder(order, bookByName);
        if(i!=1){
            returnObject.setMsg("借阅失败");
            return returnObject;
        }
        user.setRemark(user.getRemark()+order.getEndTime()+"借阅"+order.getBookName()+";");
        sqlUtil.addOrUpdateUser(user,false);
        returnObject.setData(user);
        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setMsg("借阅成功 请在"+order.getEndTime()+"之前 归还");
        return returnObject;
    }

    /**
     * 还书功能
     */
    public ReturnObject sentBook(String name) throws SQLException, ClassNotFoundException, ParseException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);

        if(" ".equals(name)){
           returnObject.setMsg("请输入正确书名");
           return returnObject;
        }

        List<Order> orders = sqlUtil.selectOrdersByUId(user.getId(), true);
        Order order = null;
        for (Order o:orders) {
            if(name.equals(o.getBookName())){
                order=new Order();
               order=o;
               break;
            }
        }

        if(order==null){
            returnObject.setMsg("未查到借阅信息，请正确输入！");
            return returnObject;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String returnTime = sdf.format(new Date());

        order.setReturnTime(returnTime);

        String endTime = order.getEndTime();


        String msg="";
        int integral=0;
        if(returnTime.compareTo(endTime)>0){
            long overTimes= sdf.parse(returnTime).getTime()-sdf.parse(endTime).getTime();

            int days = (int) TimeUnit.MILLISECONDS.toDays(overTimes);

            days=days<1?1:days;

            msg="超时"+days+"天,扣除积分"+days*Contants.INTEGRAL_DEL__TIMES;
            order.setRemark(msg);
            returnObject.setMsg(msg);
            integral=user.getIntegral()-days*Contants.INTEGRAL_DEL__TIMES;
        }
        else{
            integral=user.getIntegral()+1*Contants.INTEGRAL_ADD_TIMES_1;
            msg=returnTime+"成功归还"+order.getBookName();
            returnObject.setMsg(msg);
        }

        user.setIntegral(integral);

        user.setRemark(msg+";"+user.getRemark());


        sqlUtil.finishOneOrder(order,user);


        returnObject.setCode(Contants.USER_SUCCESS);

        returnObject.setData(user);
        return returnObject;
    }

    /**
     * 个人中心 - 签到
     */

    public ReturnObject sigInToGetIntegral() throws ParseException, SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);

        String lastSignTime = user.getLastSignTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String nowTime = sdf.format(new Date());

        if(!"".equals(lastSignTime)){
            if(nowTime.compareTo(lastSignTime)<=0){
                returnObject.setMsg("今日签过啦！");
                return returnObject;
            }
        }
        user.setLastSignTime(nowTime);
        user.setIntegral(user.getIntegral()+1*Contants.INTEGRAL_ADD_BY_SIGN_1);
        user.setRemark(user.getLastSignTime()+"签到成功 积分+"+Contants.INTEGRAL_ADD_BY_SIGN_1+";"+user.getRemark());
        sqlUtil.addOrUpdateUser(user,false);
        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setData(user);

        returnObject.setMsg("签到成功 当前积分"+user.getIntegral());

        return returnObject;
    }

    /**
     *个人中心 -获取当前用户消息
     */

    public ReturnObject getMyInfo() throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();

        String remark = sqlUtil.getUserByName(this.user.getName()).getRemark();
        if(remark.length()==0){
            returnObject.setCode(Contants.USER_ERROR);
            returnObject.setMsg("这里暂时没有消息");
            return returnObject;
        }
        String[] split = remark.split(";");

        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setData(split);
        return returnObject;
    }
    public void clearInfo() throws SQLException, ClassNotFoundException {
        user.setRemark("");
        sqlUtil.addOrUpdateUser(user,false);
    }

    public ReturnObject getOrders(Boolean isProceed) throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);

        List<Order> orders = sqlUtil.selectOrdersByUId(user.getId(), isProceed);

        String[] columnNames = new String[]{ "书名", "借阅时间","截止时间","我的评价","是否续借","原截止时间","订单状态","说明"};

        String[][] data = new String[orders.size()][columnNames.length];

        for (int i = 0; i < orders.size(); i++) {
            data[i][0]=orders.get(i).getBookName();
            data[i][1]=orders.get(i).getBorrowTime();
            data[i][2]=orders.get(i).getEndTime();
            data[i][3]=orders.get(i).getEstimate()==-1?"暂无":orders.get(i).getEstimate().toString();
            data[i][4]=orders.get(i).getIsDelay()==0? "否":"是";
            data[i][5]=orders.get(i).getOriginTime();
            data[i][6]=orders.get(i).getIsFinish()==0?"正在进行":"已完成";
            data[i][7]=orders.get(i).getRemark();
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("x",columnNames);
        map.put("y",data);
        returnObject.setData(map);

        return returnObject;
    }

    /**
     * 延期还书
     */
    public ReturnObject delayReturnBook(String bookName,int days) throws SQLException, ClassNotFoundException, ParseException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if("".equals(bookName)){
            returnObject.setMsg("请输入书名");
            return returnObject;
        }
        Order order = null;
        List<Order> orders = sqlUtil.selectOrdersByUId(user.getId(), true);
        for (Order o:orders) {
            if(o.getBookName().equals(bookName)){
                order = new Order();
                order=o;
                break;
            }
        }
        if(order==null){
            returnObject.setMsg("延期失败：未查找到相关信息");
            return returnObject;
        }
        if(order.getIsDelay()==1){
            returnObject.setMsg("只能延期一次");
            return returnObject;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = sdf.format(new Date());
        if(order.getEndTime().compareTo(nowTime)<0){
            returnObject.setMsg("已经超时，无法延期");
            return returnObject;
        }
        order.setOriginTime(order.getEndTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sdf.parse(order.getEndTime()));
        calendar.add(Calendar.DAY_OF_MONTH,days);

        order.setIsDelay(1);
        order.setEndTime(sdf.format(calendar.getTime()));
        String msg=nowTime+" "+bookName+"归还延期至 "+order.getEndTime()+";";

        user.setRemark(msg+user.getRemark());
        sqlUtil.addOrUpdateUser(user,false);

        order.setRemark(nowTime+" 延期");
        int i = sqlUtil.updateOrder(order);

        if(i!=1){
            returnObject.setMsg("系统出错");
            return returnObject;
        }

        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setMsg(msg);
        return returnObject;
    }

/**
 * 评价图书
 */

    public ReturnObject publishEvaluate(String bookName,Integer grade,String content) throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if("".equals(bookName)){
            returnObject.setMsg("请输入要评价的书名");
            return returnObject;
        }
        List<Order> orders = sqlUtil.selectOrdersByUId(user.getId(), false);
        Order order=null;
        for (Order o:orders) {
            if(o.getBookName().equals(bookName)&& o.getEstimate()==-1){
                order=new Order();
                order=o;
                break;
            }
        }
        if(order==null){
            returnObject.setMsg("需要归还 并只能评价一次");
            return returnObject;
        }

        if(!"".equals(content)){
            order.setComment(content);
        }
        order.setEstimate(grade);

        sqlUtil.updateOrder(order);

        Book bookByName = sqlUtil.getBookByName(order.getBookName());

        float v = (bookByName.getEvaluateGrade() * bookByName.getEvaluateTimes() + grade) / (bookByName.getEvaluateTimes() + 1);
        bookByName.setEvaluateTimes(bookByName.getEvaluateTimes()+1);
        bookByName.setEvaluateGrade(v);
        sqlUtil.addOrUpdateBook(bookByName,false);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        user.setRemark(sdf.format(new Date())+"完成评价;"+user.getRemark());
        user.setIntegral(user.getIntegral()+Contants.INTEGRAL_ADD_BY_EVALUATE_1);
        sqlUtil.addOrUpdateUser(user,false);
        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setMsg("评价成功");
        return returnObject;
    }
    public ReturnObject changeUserPasswd(String newPwd,String oldPwd) throws SQLException, ClassNotFoundException {

        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if(!user.getPassword().equals(oldPwd)){
            returnObject.setMsg("原密码输入错误");
            return returnObject;
        }
        if(newPwd.length()<4){
            returnObject.setMsg("新密码至少4位");
            return returnObject;
        }

        user.setPassword(newPwd);
        int i = sqlUtil.addOrUpdateUser(user, false);
        if(i==1){
            returnObject.setCode(Contants.USER_SUCCESS);
            returnObject.setMsg("修改成功!");
            return returnObject;
        }
        returnObject.setMsg("修改失败");
        return returnObject;
    }
    public  ReturnObject getMsgSquare() throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();

        List<MsgSquare> msg = sqlUtil.getMsg();
        if(msg.size()==0){
            returnObject.setCode(Contants.USER_ERROR);
            returnObject.setMsg("这里暂时没有消息");
            return returnObject;
        }
        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setData(msg);
        return returnObject;
    }
};
