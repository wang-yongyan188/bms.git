package primary.service;

import primary.pojo.Book;
import primary.pojo.Order;
import primary.pojo.User;
import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.utils.SqlUtil;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
public class MangerOperator {
    private User user;
    private SqlUtil sqlUtil;
    public MangerOperator(User user) {
        this.user = user;
        this.sqlUtil=new SqlUtil();
    }

    public ReturnObject addBook(String bookName, String bookType, Integer totalNum) throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if(totalNum<=0){
            returnObject.setMsg("请正确填写库存数量");
            return returnObject;
        }
        Book bookByName = sqlUtil.getBookByName(bookName);
        if(bookByName!=null){
            returnObject.setMsg("该书在系统中已存在");
            return returnObject;
        }
        Book book = new Book();
        book.setBookName(bookName);
        book.setBookType(bookType);
        book.setTotalNum(totalNum);

        int i = sqlUtil.addOrUpdateBook(book, true);
        if(i==1){
            sqlUtil.addMsg("添加图书"+bookName);
            returnObject.setCode(Contants.USER_SUCCESS);
            returnObject.setMsg("添加成功");
            return returnObject;
        }

        returnObject.setMsg("添加失败");
        return returnObject;
    }

    public ReturnObject delBook(String bookName) throws SQLException, ClassNotFoundException{
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if("".equals(bookName)){
            returnObject.setMsg("请正确填写书名");
            return returnObject;
        }

        List<Order> orders = sqlUtil.selectOrdersByBooKName(bookName, true);
        if(orders.size()>0){
            returnObject.setMsg("需要等该书归还后，在删除");
            return returnObject;
        }
        int i = sqlUtil.delBook(bookName);
        if(i==1){
            sqlUtil.addMsg("删除图书"+bookName);
            returnObject.setCode(Contants.USER_SUCCESS);
            returnObject.setMsg("删除成功");
            return returnObject;
        }

        returnObject.setMsg("删除失败");
        return returnObject;
    }

    /**
     * 在更新时 查找数据 并返回页面
     */
    public ReturnObject getBookByName(String bookName) throws SQLException, ClassNotFoundException{
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if("".equals(bookName)){
            returnObject.setMsg("请正确填写书名");
            return returnObject;
        }

        Book bookByName = sqlUtil.getBookByName(bookName);
        if(bookByName!=null){

            returnObject.setCode(Contants.USER_SUCCESS);
            returnObject.setData(bookByName);
            return returnObject;
        }

        returnObject.setMsg("查找失败");
        return returnObject;
    }
    public ReturnObject updateBookById(String bookName,String bookType,Integer totalNum,Integer id) throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);

        if("".equals(bookName)||totalNum<=0){
            returnObject.setMsg("请正确填写信息");
            return returnObject;
        }
        Book bookById = sqlUtil.getBookById(id);

        if(!bookById.getBookName().equals(bookName)){
            Book bookByName = sqlUtil.getBookByName(bookName);
            if(bookByName!=null){
                returnObject.setMsg("g该书名在系统中已经存在!");
                return returnObject;
            }
        }

        if(bookById.getLendNum()>totalNum){
            returnObject.setMsg("书的总数量不能小于借出数量");
            return returnObject;
        }

        bookById.setBookName(bookName);
        bookById.setBookType(bookType);
        bookById.setTotalNum(totalNum);
        int i = sqlUtil.updateBookById(bookById);
        if(i==1){
            sqlUtil.addMsg("修改图书"+bookById.getBookName());
            returnObject.setCode(Contants.USER_SUCCESS);
            returnObject.setMsg("修改成功");
            return returnObject;
        }
        returnObject.setMsg("修改失败");

        return returnObject;
    }
    public ReturnObject getAllBooksByCondition(Boolean isTotal,Boolean isEva,Boolean isTimes) throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);

        List<Book> books = sqlUtil.getAllBooksByCondition(isTotal, isEva, isTimes);

        if(books.size()==0){
            returnObject.setMsg("系统还没有数据");
            return returnObject;
        }

        HashMap<String, Object> map = new HashMap<>();
        String[] x=new String[]{"图书类型","书的总数","借出数量","剩余库存","评价条数","评价分数","借出次数"};
        map.put("x",x);

        String[][] y=new String[books.size()][7];
        for (int i=0;i<books.size();i++){
            y[i][0]=books.get(i).getBookType();
            y[i][1]=books.get(i).getTotalNum().toString();
            y[i][2]=books.get(i).getLendNum().toString();
            y[i][3]= String.valueOf(books.get(i).getTotalNum()-books.get(i).getLendNum());
            y[i][4]=books.get(i).getEvaluateTimes().toString();
            y[i][5]=books.get(i).getEvaluateGrade()+"分";
            y[i][6]=books.get(i).getLendNum()+" 人次";
        }

        map.put("y" ,y);


        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setData(map);
        return returnObject;
    }
}
