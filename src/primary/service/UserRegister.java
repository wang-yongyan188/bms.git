package primary.service;

import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.pojo.User;
import primary.utils.SqlUtil;
import java.sql.SQLException;

public class UserRegister {
    private String name;
    private String password;

    public UserRegister(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public ReturnObject toReg() throws SQLException, ClassNotFoundException {
        ReturnObject returnObject = new ReturnObject();
        returnObject.setCode(Contants.USER_ERROR);
        if(this.name==null||"".equals(this.name)){
            returnObject.setMsg("请正确输入用户名");
            return returnObject;
        }
        if(this.password==null||"".equals(this.password)){
            returnObject.setMsg("密码不能为空");
            return returnObject;
        }

        SqlUtil sqlUtil = new SqlUtil();
        User userByName = sqlUtil.getUserByName(this.name);
        if(userByName!=null){
            returnObject.setMsg("该用户已存在");
            return returnObject;
        }
        User user = new User();
        user.setName(this.name);
        user.setPassword(this.password);
        int i = sqlUtil.addOrUpdateUser(user, true);

        if(i==0){
            returnObject.setMsg("系统出错");
            return returnObject;
        }

        returnObject.setCode(Contants.USER_SUCCESS);
        returnObject.setMsg("注册成功，请重新登录");
        return returnObject;
    }
}
