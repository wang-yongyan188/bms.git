package primary.service;

import primary.pojo.Order;
import primary.pojo.common.Contants;
import primary.pojo.common.ReturnObject;
import primary.pojo.User;
import primary.utils.SqlUtil;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class UserVerify {

    private String name;
    private String password;
    private SqlUtil sqlUtils;
    private User user;

    public UserVerify(String name, String password) {
        this.name = name;
        this.password = password;
        this.sqlUtils=new SqlUtil();
    }
    private Boolean isLegal(){
        if("".equals(this.name)|| "".equals(this.password)||this.password==null||this.name==null) return true;
        return false;
    }


    private Boolean isExist() throws SQLException, ClassNotFoundException {

        User user = sqlUtils.getUserByName(this.name);

        if(user==null) return true;
        this.user=user;
        return false;
    }

    private Boolean isCorrect(){
        if(user.getPassword().equals(this.password)) return true;
        return false;
    }

    public ReturnObject toLogin() throws Exception {
        ReturnObject object = new ReturnObject();
        object.setCode(Contants.USER_ERROR);

        if(isLegal()){
            object.setMsg("用户名或密码不能为空");
            return object;
        }
        if(isExist()){
            object.setMsg("该用户不存在，请先注册");
            return object;
        }
        if(isExist()){
            object.setMsg("该用户不存在，请先注册");
            return object;
        }
        if(!isCorrect()){
            object.setMsg("密码错误");
            return object;
        }

        if(this.user.getIsManger()==1){
            object.setCode(Contants.MANGER_SUCCESS);
            object.setMsg("欢迎管理员"+this.user.getName());
            return object;
        }
        object.setCode(Contants.USER_SUCCESS);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        List<Order> orders = sqlUtils.selectOrdersByUId(user.getId(), true);
        StringBuffer buffer = new StringBuffer("欢迎使用 图书管理系统@YAN ");
        for (Order o:orders) {
            if(sdf.parse(o.getEndTime()).getTime()-sdf.parse(o.getBorrowTime()).getTime()<=Contants.ONE_DAY){
              buffer.append(" 请及时归还 "+o.getBookName()+" !");
            }
            if(user.getRemark().length()>230){
                buffer.append("消息空间不足，请及时清理!");
            }
        }

        object.setMsg(buffer.toString());
        object.setData(this.user);

        return object;
    }
}
