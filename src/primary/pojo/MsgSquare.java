package primary.pojo;

public class MsgSquare {
    private Integer id;
    private String time;
    private String msg;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "MsgSquare{" +
                "id=" + id +
                ", time='" + time + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
