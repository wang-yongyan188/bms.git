package primary.pojo;

public class Order {

    private String id;

    private Integer userId;

    private Integer bookId;

    private String borrowTime;

    private String endTime;

    private String returnTime;

    private Integer estimate;

    private String comment;

    private Integer isDelay;

    private String originTime;

    private Integer isFinish;

    private String bookName;


    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getBorrowTime() {
        return borrowTime;
    }

    public void setBorrowTime(String borrowTime) {
        this.borrowTime = borrowTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getIsDelay() {
        return isDelay;
    }

    public void setIsDelay(Integer isDelay) {
        this.isDelay = isDelay;
    }

    public String getOriginTime() {
        return originTime;
    }

    public void setOriginTime(String originTime) {
        this.originTime = originTime;
    }

    public Integer getEstimate() {
        return estimate;
    }

    public void setEstimate(Integer estimate) {
        this.estimate = estimate;
    }

    public Integer getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(Integer isFinish) {
        this.isFinish = isFinish;
    }

    public String getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(String returnTime) {
        this.returnTime = returnTime;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                ", bookId=" + bookId +
                ", borrowTime='" + borrowTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", returnTime='" + returnTime + '\'' +
                ", estimate=" + estimate +
                ", comment='" + comment + '\'' +
                ", isDelay=" + isDelay +
                ", originTime='" + originTime + '\'' +
                ", isFinish=" + isFinish +
                ", bookName='" + bookName + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}