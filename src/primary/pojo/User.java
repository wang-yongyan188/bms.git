package primary.pojo;

public class User {
    private Integer id;

    private String name;

    private String password;

    private Integer isManger;

    private String remark;

    private Integer integral;

    private String lastSignTime;

    public String getLastSignTime() {
        return lastSignTime;
    }

    public void setLastSignTime(String lastSignTime) {
        this.lastSignTime = lastSignTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIsManger() {
        return isManger;
    }

    public void setIsManger(Integer isManger) {
        this.isManger = isManger;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", isManger=" + isManger +
                ", remark='" + remark + '\'' +
                ", integral=" + integral +
                ", lastSignTime='" + lastSignTime + '\'' +
                '}';
    }
}