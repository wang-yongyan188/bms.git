package primary.pojo.common;

public interface Contants {

    public static final int USER_ERROR=0;

    public static final int USER_SUCCESS=1;

    public static final int MANGER_SUCCESS=2;

    public static final int SYS_SUCCESS=3;

    public static final int INTEGRAL_DEL__TIMES=1;
    public static final int INTEGRAL_DEL_TIMES_2=2;
    public static final int INTEGRAL_ADD_TIMES_1=1;
    public static final int INTEGRAL_ADD_BY_SIGN_1=1;
    public static final int INTEGRAL_ADD_BY_EVALUATE_1=1;
    public static final Long ONE_DAY=21*3600*1000L;



}
