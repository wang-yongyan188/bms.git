package primary.pojo;

public class Book {
    private Integer id;

    private String bookName;

    private String bookType;

    private Integer totalNum;

    private Integer lendNum;

    private Float evaluateGrade;

    private Integer lendTimes;

    private Integer evaluateTimes;

    public Integer getLendTimes() {
        return lendTimes;
    }

    public void setLendTimes(Integer lendTimes) {
        this.lendTimes = lendTimes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }



    public Float getEvaluateGrade() {
        return evaluateGrade;
    }

    public void setEvaluateGrade(Float evaluateGrade) {
        this.evaluateGrade = evaluateGrade;
    }

    public Integer getLendNum() {
        return lendNum;
    }

    public void setLendNum(Integer lendNum) {
        this.lendNum = lendNum;
    }

    public Integer getEvaluateTimes() {
        return evaluateTimes;
    }

    public void setEvaluateTimes(Integer evaluateTimes) {
        this.evaluateTimes = evaluateTimes;
    }
    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", bookType='" + bookType + '\'' +
                ", totalNum=" + totalNum +
                ", lendNum=" + lendNum +
                ", evaluateGrade=" + evaluateGrade +
                ", lendTimes=" + lendTimes +
                ", evaluateTimes=" + evaluateTimes +
                '}';
    }
}